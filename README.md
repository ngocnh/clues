FORMAT: 1A

# API Documents

# User [/user]
User resource representation.

## Login. [POST /user/user/login]
Login with account username or email and account password

+ Parameters
    + username (string, required) - Account username or email
    + password (string, required) - Account password

+ Request (application/json)
    + Body

            {
                "username": "user01@local.app",
                "password": "123456"
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 5,
                    "email": "user01@local.app",
                    "roles": {
                        "3": {
                            "id": 3,
                            "name": "test",
                            "display_name": "Test",
                            "description": "test"
                        }
                    },
                    "type": "default",
                    "status": 1,
                    "created_at": {
                        "date": "2015-09-02 00:49:02.000000",
                        "timezone_type": 3,
                        "timezone": "Asia/Ho_Chi_Minh"
                    }
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "<strong>Error!</strong> User <strong><i>user01@local.app</i></strong> not found.",
                "status_code": 422
            }

## Show all users [GET /user{?page,limit}]
Get a JSON representation of all the registered users.

+ Parameters
    + page (integer, optional) - The page of results to view.
        + Default: 1
    + limit (integer, optional) - The amount of results per page.
        + Default: 10

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 1,
                        "email": "user01@local.app",
                        "roles": {
                            "1": {
                                "id": 1,
                                "name": "member",
                                "display_name": "Member",
                                "description": "Member"
                            }
                        },
                        "type": "default",
                        "status": 1,
                        "created_at": {
                            "date": "2015-08-29 01:58:30.000000",
                            "timezone_type": 3,
                            "timezone": "Asia/Ho_Chi_Minh"
                        }
                    },
                    {
                        "id": 1,
                        "email": "user01@local.app",
                        "roles": {
                            "1": {
                                "id": 1,
                                "name": "Member",
                                "display_name": "Member",
                                "description": "Member"
                            }
                        },
                        "type": "default",
                        "status": 1,
                        "created_at": {
                            "date": "2015-08-29 01:58:30.000000",
                            "timezone_type": 3,
                            "timezone": "Asia/Ho_Chi_Minh"
                        }
                    }
                ],
                "meta": {
                    "pagination": {
                        "total": 2,
                        "count": 2,
                        "per_page": 10,
                        "current_page": 1,
                        "total_pages": 1,
                        "links": ""
                    }
                }
            }

## Register user [POST /user]
Register a new user with a `username` and `password`.

+ Parameters
    + username (string, optional) - Account username
    + email (string, required) - Account email address
    + password (string, required) - Account password
    + password_confirm (string, required) - Password confirmed
    + meta (json, optional) - Account meta, any data by key:value , etc: 'first_name': 'A'

+ Request (application/json)
    + Body

            {
                "email": "user03@local.app",
                "password": "secret",
                "password_confirm": "secret",
                "label_id": 1,
                "member_type": 1,
                "nickname": "User"
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 3,
                    "email": "user03@local.app",
                    "roles": {
                        "1": {
                            "id": 1,
                            "name": "member",
                            "display_name": "Member",
                            "description": "Member"
                        }
                    },
                    "type": "default",
                    "status": 1,
                    "created_at": {
                        "date": "2015-08-29 01:58:30.000000",
                        "timezone_type": 3,
                        "timezone": "Asia/Ho_Chi_Minh"
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Resource validation failed!",
                "errors": {
                    "username": [
                        "The username has already been taken.",
                        "The username must be between 4 and 24 characters."
                    ],
                    "email": [
                        "The email field is required.",
                        "The email has already been taken.",
                        "The email may not be greater than 128 characters."
                    ],
                    "password": [
                        "The password field is required.",
                        "The password must be at least 6 characters."
                    ],
                    "password_confirm": [
                        "The password confirm field is required.",
                        "The password must be at least 6 characters."
                    ],
                    "meta": [
                        "The meta must be an json."
                    ]
                },
                "status_code": 422
            }

## Get user. [GET /user/{id}]
Get a user by user id

+ Parameters
    + id (integer, required) - User ID

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 5,
                    "email": "user01@local.app",
                    "roles": {
                        "3": {
                            "id": 3,
                            "name": "test",
                            "display_name": "Test",
                            "description": "test"
                        }
                    },
                    "type": "default",
                    "status": 1,
                    "created_at": {
                        "date": "2015-09-02 00:49:02.000000",
                        "timezone_type": 3,
                        "timezone": "Asia/Ho_Chi_Minh"
                    }
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "<strong>Error!</strong> User <strong><i>6</i></strong> not found.",
                "status_code": 422
            }

## Update user [PUT /user/{id}]
Update a user by user id or change new password for user

+ Parameters
    + id (integer, required) - User id
    + username (string, optional) - New username
    + new_password (string, optional) - If user change password, set new_password for
     *                                 change
    + password_confirm (string, optional) - Password confirmed
    + meta (json, optional) - Account meta, any data by key:value , etc: 'first_name':
     *                         'A'

+ Request (application/json)
    + Body

            {
                "username": "user005",
                "new_password": "1234567",
                "password_confirm": "1234567"
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 5,
                    "email": "user01@local.app",
                    "roles": {
                        "3": {
                            "id": 3,
                            "name": "test",
                            "display_name": "Test",
                            "description": "test"
                        }
                    },
                    "type": "default",
                    "status": 0,
                    "created_at": {
                        "date": "2015-09-02 00:49:02.000000",
                        "timezone_type": 3,
                        "timezone": "Asia/Ho_Chi_Minh"
                    }
                }
            }

+ Response 403 (application/json)
    + Body

            {
                "message": "Access denied!",
                "status_code": 403
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "Resource validation failed!",
                "errors": {
                    "username": [
                        "The username has already been taken.",
                        "The username must be between 4 and 24 characters."
                    ],
                    "new_password": [
                        "The new password must be at least 6 characters."
                    ],
                    "password_confirm": [
                        "The password confirm and new password must match.",
                        "The password confirm must be at least 6 characters."
                    ],
                    "meta": [
                        "The meta must be an json."
                    ]
                }
            }

# Client [/client]

## Get access token [POST /client/access_token]
Get a JSON representation of access token by client id and secret, if grant type is password, email (username)
and password is required.

+ Parameters
    + grant_type (string, required) - Grant type by 'password' or
     *                               'client_credentials'
    + client_id (string, required) - Registered client id
    + client_secret (string, required) - Registered client secret
    + username (string, optional) - Username required if grant type is 'password'
    + password (string, optional) - Password required if grant type is 'password'

+ Request Grant by 'client_credentials' (application/json)
    + Body

            {
                "grant_type": "client_credentials",
                "client_id": "VmUXPLVP4oTsk2jY",
                "client_secret": "UgozNlpaCQ5wtjtnIJkst6j6pCRPyGys"
            }

+ Request Grant by 'password' (application/json)
    + Body

            {
                "grant_type": "password",
                "client_id": "VmUXPLVP4oTsk2jY",
                "client_secret": "UgozNlpaCQ5wtjtnIJkst6j6pCRPyGys",
                "username": "user01@local.app",
                "password": "secret"
            }

+ Response 200 (application/json)
    + Body

            {
                "access_token": "MMg6nTjWwllqAWtqugKqfWeVfQsAj92zhkVg0p9O",
                "token_type": "Bearer",
                "expires_in": 3600
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Client authentication failed.",
                "status_code": 500
            }

## Get User by Access Token [GET /client/user]
Get user information by access token

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 1,
                    "username": "administrator",
                    "email": "me@ngocnh.info",
                    "activation_key": null,
                    "last_visited": "2015-09-14 16:30:26",
                    "type": "default",
                    "status": 1,
                    "created_at": {
                        "date": "2015-09-14 15:49:49.000000",
                        "timezone_type": 3,
                        "timezone": "Asia/Ho_Chi_Minh"
                    },
                    "roles": [
                        {
                            "id": 1,
                            "name": "super-administrator",
                            "display_name": "Super Administrator",
                            "description": "Super Administrator",
                            "default": 0,
                            "permissions": [
                                {
                                    "access.all": "All Permissions"
                                }
                            ]
                        }
                    ],
                    "meta": {
                        "fullname": "Super Administrator"
                    }
                }
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Unauthorized.",
                "status_code": 401
            }